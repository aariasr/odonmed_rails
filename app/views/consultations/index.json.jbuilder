json.array!(@consultations) do |consultation|
  json.extract! consultation, :id, :patient_id, :hour_id, :status
  json.url consultation_url(consultation, format: :json)
end
