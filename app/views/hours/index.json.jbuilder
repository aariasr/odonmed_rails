json.array!(@hours) do |hour|
  json.extract! hour, :id, :medic_id, :day, :hour
  json.url hour_url(hour, format: :json)
end
