json.array!(@medics) do |medic|
  json.extract! medic, :id, :name, :speciality, :dni, :user
  json.url medic_url(medic, format: :json)
end
