json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :last_name, :email, :phone, :references
  json.url patient_url(patient, format: :json)
end
