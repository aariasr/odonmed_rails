jQuery ->
	$("#datepicker").datepicker();
	$("#datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
	$('#ui-datepicker-div').hide()
	$("#datepicker").click ->
		$('#ui-datepicker-div').show()
	$("#datepicker2").datepicker();
	$("#datepicker2").datepicker("option", "dateFormat", "yy-mm-dd");
	$('#ui-datepicker-div').hide()
	$("#datepicker2").click ->
		$('#ui-datepicker-div').show()

jQuery ->
	date = new Date();
	day = date.getDay();
	$('#multi').multiDatesPicker({beforeShowDay: $.datepicker.noWeekends})

jQuery ->
	$('#medic_id').change ->
		$('#datepicker').attr("disabled", true)
		if $('#medic_id').val() != ""
			$('#datepicker').attr("disabled", false)

jQuery ->
	$('#datepicker').change ->
		$.getJSON '/getHours/' + $('#datepicker').val() + '.json', (results) ->
			$('#horas').val('')
			$('#horas').empty()
			options = $('#horas')
			$('#horas').attr('disabled', 'disabled')
			$('.error-hora').show()
			for hora in results
				seleccion = $('#medic_id').val()
				if parseInt(hora.medic_id) == parseInt(seleccion) && hora.status == "disponible"
					$('#horas').removeAttr("disabled")
					options.append $('<option />').val(hora.id).text(hora.hour)
					$('.error-hora').hide()