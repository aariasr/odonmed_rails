# == Schema Information
#
# Table name: hours
#
#  id         :integer          not null, primary key
#  medic_id   :integer
#  day        :date
#  hour       :string
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hours_on_medic_id  (medic_id)
#

class Hour < ActiveRecord::Base
  belongs_to :medic
  belongs_to :consultation

end
