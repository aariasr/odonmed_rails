# == Schema Information
#
# Table name: medics
#
#  id         :integer          not null, primary key
#  name       :string
#  speciality :string
#  dni        :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_medics_on_user_id  (user_id)
#

class Medic < ActiveRecord::Base
  belongs_to :user
  has_many :hours
  validates_presence_of :name, :dni, :speciality, :user
end
