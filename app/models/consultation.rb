# == Schema Information
#
# Table name: consultations
#
#  id         :integer          not null, primary key
#  patient_id :integer
#  hour_id    :integer
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_consultations_on_hour_id     (hour_id)
#  index_consultations_on_patient_id  (patient_id)
#

class Consultation < ActiveRecord::Base
  belongs_to :patient
  belongs_to :hour
end
