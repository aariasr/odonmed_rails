class HoursController < ApplicationController
  before_action :set_hour, only: [:show, :edit, :update, :destroy]

  # GET /hours
  # GET /hours.json
  def index
    @hours = Hour.all
  end

  # GET /hours/1
  # GET /hours/1.json
  def show
  end

  # GET /hours/new
  def new
    @hour = Hour.new
    @times = create_hours(:start_time => 6.hours, :end_time => 20.hours)
  end

  # GET /hours/1/edit
  def edit
    @times = create_hours(:start_time => 6.hours, :end_time => 20.hours)
  end

  # POST /hours
  # POST /hours.json
  def create
    @days = params[:hour][:day].split(/\s*,\s*/)
    @hours = params[:hour][:hour]
    @days.each do |d|
      @hours.each do |h|
        if h.present?
          @hour = Hour.new(hour_params)
          @hour.day = d
          @hour.hour = h
          @hour.save
        end
      end
    end

    respond_to do |format|
      if @hour.save
        format.html { redirect_to '/dashboard', notice: 'Hora agregada.' }
        format.json { render :show, status: :created, location: @hour }
      else
        format.html { render :new }
        format.json { render json: @hour.errors, status: :unprocessable_entity }
      end
    end 
    
  end

  # PATCH/PUT /hours/1
  # PATCH/PUT /hours/1.json
  def update
    respond_to do |format|
      if @hour.update(hour_params)
        format.html { redirect_to @hour, notice: 'Hour was successfully updated.' }
        format.json { render :show, status: :ok, location: @hour }
      else
        format.html { render :edit }
        format.json { render json: @hour.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hours/1
  # DELETE /hours/1.json
  def destroy
    @hour.destroy
    respond_to do |format|
      format.html { redirect_to hours_url, notice: 'Hour was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def days 
    @days = Hour.where(medic_id: params[:medic_id])
    respond_to do |format|
      format.html
      format.json { render json: @days }
    end
  end

  def hours
    @hours = Hour.where(day: params[:day])
    respond_to do |format|
      format.html
      format.json { render json: @hours }
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hour
      @hour = Hour.find(params[:id])
    end

    def create_hours(parameters)
      start_time = parameters[:start_time] ? parameters[:start_time] : 0
      end_time = parameters[:end_time] ? parameters[:end_time] : 24.hours
      increment = parameters[:increment] ? parameters[:increment] : 45.minutes
      Array.new(1 + (end_time - start_time)/increment) do |i|
          (Time.now.midnight + (i*increment) + start_time).strftime("%I:%M %p")
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hour_params
      params.require(:hour).permit(:medic_id, :day, :hour)
    end
end
