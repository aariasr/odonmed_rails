class DashboardsController < ApplicationController
	before_action :authenticate_user!
	before_action :create_profile, only: :show
	layout 'dashboard'

	def index
	end

	def show
		@user = current_user
		@medics = Medic.all
	end

	def reserva
		@consultation = Consultation.new
		@medics = Medic.all
		@hours = create_hours(:start_time => 6.hours, :end_time => 20.hours)
		@patient = Patient.find_by(user_id: current_user.id)
	end

	def med_hours
		@hour = Hour.new
    	@times = create_hours(:start_time => 6.hours, :end_time => 20.hours)
	end

	def medics
		@medics = Medic.all
	end

	def my_hours
		@consultation = Consultation.where(:patient_id => current_user.id)
	end

	def patient_profile
		@patient = Patient.new
	end

	def create_hours(parameters)
		start_time = parameters[:start_time] ? parameters[:start_time] : 0
		end_time = parameters[:end_time] ? parameters[:end_time] : 24.hours
		increment = parameters[:increment] ? parameters[:increment] : 45.minutes
		Array.new(1 + (end_time - start_time)/increment) do |i|
	    	(Time.now.midnight + (i*increment) + start_time).strftime("%I:%M %p")
		end
	end

	def create_profile
		if current_user.tipo == '2' || Patient.find_by(user_id: current_user.id).nil?
			redirect_to '/patient_profile'
		end
	end
end
