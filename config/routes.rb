Rails.application.routes.draw do
  resources :consultations
  resources :hours
  resources :patients
  resources :medics
  devise_for :users
	root 'welcome#index'
	resource 'dashboard'
	

	authenticated :user do
		get 'dashboard/show' => 'dashboards#show'
		get 'my_hours' => 'dashboards#my_hours'
		get 'medic' => 'dashboards#medics'
		get 'med_hours' => 'dashboards#med_hours'
		get 'reserva' => 'dashboards#reserva'
		get 'getDays/:medic_id' => 'hours#days'
		get 'getHours/:day' => 'hours#hours'
		get 'patient_profile' => 'dashboards#patient_profile'
	end

	unauthenticated :user do
		devise_scope :user do
			get 'sessions/new' => 'sessions#new', as: :unregistered_root
		end
	end
	
	

end
