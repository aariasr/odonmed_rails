class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.references :medic, index: true, foreign_key: true
      t.date :day
      t.string :hour
      t.boolean :status

      t.timestamps null: false
    end
  end
end
