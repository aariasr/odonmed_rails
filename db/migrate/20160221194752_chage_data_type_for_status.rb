class ChageDataTypeForStatus < ActiveRecord::Migration
  def self.up
    change_table :hours do |t|
      t.change :status, :string
    end
  end
  def self.down
    change_table :hours do |t|
      t.change :status, :boolean
    end
  end
end
