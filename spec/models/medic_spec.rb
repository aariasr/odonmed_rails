require 'rails_helper'

RSpec.describe Medic, type: :model do
  it { should validate_presence_of :name }
  it { should validate_presence_of :speciality }
  it { should validate_presence_of :dni }
  it { should validate_presence_of :user }
end
