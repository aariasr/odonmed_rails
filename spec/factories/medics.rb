FactoryGirl.define do
  factory :medic do
    name "MyString"
    speciality "MyString"
    dni "MyString"
    association :user, factory: :user
  end
end
