FactoryGirl.define do
  factory :patient do
    name "MyString"
    last_name "MyString"
    email "MyString"
    phone "MyString"
    references ""
  end
end
