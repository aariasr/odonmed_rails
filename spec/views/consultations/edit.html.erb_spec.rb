require 'rails_helper'

RSpec.describe "consultations/edit", type: :view do
  before(:each) do
    @consultation = assign(:consultation, Consultation.create!(
      :patient => nil,
      :hour => nil,
      :status => "MyString"
    ))
  end

  it "renders the edit consultation form" do
    render

    assert_select "form[action=?][method=?]", consultation_path(@consultation), "post" do

      assert_select "input#consultation_patient_id[name=?]", "consultation[patient_id]"

      assert_select "input#consultation_hour_id[name=?]", "consultation[hour_id]"

      assert_select "input#consultation_status[name=?]", "consultation[status]"
    end
  end
end
