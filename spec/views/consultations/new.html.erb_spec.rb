require 'rails_helper'

RSpec.describe "consultations/new", type: :view do
  before(:each) do
    assign(:consultation, Consultation.new(
      :patient => nil,
      :hour => nil,
      :status => "MyString"
    ))
  end

  it "renders new consultation form" do
    render

    assert_select "form[action=?][method=?]", consultations_path, "post" do

      assert_select "input#consultation_patient_id[name=?]", "consultation[patient_id]"

      assert_select "input#consultation_hour_id[name=?]", "consultation[hour_id]"

      assert_select "input#consultation_status[name=?]", "consultation[status]"
    end
  end
end
