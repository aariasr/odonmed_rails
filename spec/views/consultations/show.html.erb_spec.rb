require 'rails_helper'

RSpec.describe "consultations/show", type: :view do
  before(:each) do
    @consultation = assign(:consultation, Consultation.create!(
      :patient => nil,
      :hour => nil,
      :status => "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Status/)
  end
end
