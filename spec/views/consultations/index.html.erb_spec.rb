require 'rails_helper'

RSpec.describe "consultations/index", type: :view do
  before(:each) do
    assign(:consultations, [
      Consultation.create!(
        :patient => nil,
        :hour => nil,
        :status => "Status"
      ),
      Consultation.create!(
        :patient => nil,
        :hour => nil,
        :status => "Status"
      )
    ])
  end

  it "renders a list of consultations" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
