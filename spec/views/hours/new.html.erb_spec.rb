require 'rails_helper'

RSpec.describe "hours/new", type: :view do
  before(:each) do
    assign(:hour, Hour.new(
      :medic => nil
    ))
  end

  it "renders new hour form" do
    render

    assert_select "form[action=?][method=?]", hours_path, "post" do

      assert_select "input#hour_medic_id[name=?]", "hour[medic_id]"
    end
  end
end
