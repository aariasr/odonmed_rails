require 'rails_helper'

RSpec.describe "patients/index", type: :view do
  before(:each) do
    assign(:patients, [
      Patient.create!(
        :name => "Name",
        :last_name => "Last Name",
        :email => "Email",
        :phone => "Phone",
        :references => ""
      ),
      Patient.create!(
        :name => "Name",
        :last_name => "Last Name",
        :email => "Email",
        :phone => "Phone",
        :references => ""
      )
    ])
  end

  it "renders a list of patients" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
