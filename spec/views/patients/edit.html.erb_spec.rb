require 'rails_helper'

RSpec.describe "patients/edit", type: :view do
  before(:each) do
    @patient = assign(:patient, Patient.create!(
      :name => "MyString",
      :last_name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :references => ""
    ))
  end

  it "renders the edit patient form" do
    render

    assert_select "form[action=?][method=?]", patient_path(@patient), "post" do

      assert_select "input#patient_name[name=?]", "patient[name]"

      assert_select "input#patient_last_name[name=?]", "patient[last_name]"

      assert_select "input#patient_email[name=?]", "patient[email]"

      assert_select "input#patient_phone[name=?]", "patient[phone]"

      assert_select "input#patient_references[name=?]", "patient[references]"
    end
  end
end
